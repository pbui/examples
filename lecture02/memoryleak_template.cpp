// memoryleak.cpp: where is my mind

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>

const int NITEMS = 1<<10;
const int TRIALS = 100;

using namespace std;

template <typename T>
class Array {
public:
    typedef T* iterator;

    Array(const int n) : length(n), data(new T[n]) {}
    ~Array()			    { delete [] data; }
    const size_t size() const	    { return length; }
    T& operator[](const int i)	    { return data[i]; }
    iterator begin()		    { return data; }
    iterator end()		    { return data + length; }

private:
    size_t  length;
    T*	    data;
};

bool duplicates(int n) {
    Array<int> randoms(n);

    for (int i = 0; i < NITEMS; i++) {
    	randoms[i] = rand() % 1000;
    }

    for (int i = 0; i < n; i++) {
	if (find(randoms.begin(), randoms.end(), randoms[i]) != randoms.end()) {
	    return true;
	}
    }

    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
	    cout << "Duplicates Detected!" << endl;
	}
    }

    return 0;
}
