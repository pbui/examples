// sieve_array.cpp

#include <cmath>
#include <cstdint>
#include <iostream>

const size_t N = 1<<20;

using namespace std;

int main(int argc, char *argv[]) {
    bool numbers[N];

    // Initialize numbers
    for (size_t i = 1; i < N; i++) {
        numbers[i] = true;
    }

    // Sieve
    for (size_t i = 2; i < (size_t)(sqrt(N)); i++) {
        if (numbers[i]) {
            for (size_t p = i*i; p < N; p += i) {
                numbers[p] = false;
            }
        }
    }
    
    // Output
    size_t count = 0;
    for (size_t i = 1; i < N; i++) {
    	if (numbers[i]) {
    	    count++;
    	    //cout << i << endl;
	}
    }
    cout << count << endl;

    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
