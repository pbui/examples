// stack_default.cpp

#include <cassert>
#include <stack>

using namespace std;

const int N = 1<<28;

int main(int argc, char *argv[]) {
    stack<int> s;

    // Fill stack
    for (int i = 0; i < N; i++) {
    	s.push(i);
    }

    // Empty stack
    int i = N - 1;
    while (!s.empty()) {
    	assert(s.top() == i);
    	s.pop();
    	i--;
    }

    return 0;
}
